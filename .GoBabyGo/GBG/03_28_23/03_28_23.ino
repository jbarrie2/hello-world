// || Dr. J Ventura || Juan Barrientos
// sets the input and output signals to proper pins 
const int PulseInA = 3;                       
const int PulseInB = 5;
const int carpwm = 9;
const int cardir =11;

// Defines the int & const int values
int countA;
int countB;
int delayUsec = 5;
int pulseNumber = 20;

const int systemDelay = 100;
const int carAcc = 40;                          // Car Acceleration Delay
const int longDelay = 2000;
const int topSpeed = 100;                       // Limiting car speed
int carSpeed = 0;
const int minSpeed =0;
//int accIndex=10;                              // Acceleration index increment Not used 4-18-22


const int carZero = 0;                          // Car speed zero
//const int controllerHigh=300; //
//const int controllerLow=300; //
int serialOn = 1;                               // Serial Monitor - On if "1" Added 4-17-22

void setup() {
  //  serialOn = 0;                             // Serial Monitor - On if "1" Added 4-17-22
  pinMode(PulseInA, INPUT);
  pinMode(PulseInB, INPUT);
  pinMode(carpwm, OUTPUT);
  pinMode(cardir, OUTPUT);
  Serial.begin(9600);

}

void loop() {
  carSpeed = carZero;                           // Making car speed zero initially
  analogWrite(carpwm, carZero);
  
  countA = highsPercycle1();
  countB = highsPercycle2();
  delay(systemDelay);

  while (countA < 1 && countB < 1 ) {           // BOTH SWITCHES ARE OFF (6-8-22)


    if (serialOn > 0) {                         // This IF statement prints out carSpeed, CountA & CountB values if TRUE

      Serial.print(" carSpeed value is ");
      Serial.print(carSpeed);
      
      Serial.print("  , CountA = ");
      Serial.print(countA);
      //Serial.print("\n");
      Serial.print(" , CountB = ");
      Serial.print(countB); 
      //Serial.print("\n");
      //Serial.print(" , Dir: ");
      //Serial.print(cardir);
      Serial.println();
    }
    if (carSpeed > minSpeed)
    {
      carSpeed--;
      analogWrite(carpwm,carSpeed);
    }
      
countA = highsPercycle1();                  // Checks the highValue again, if changed then the while loop will be exited
countB = highsPercycle2();

    delay(carAcc);
    if (carSpeed <= minSpeed){
      carSpeed = minSpeed; 
    }
   }
  
  carSpeed = carZero;                           // Initally sets the carSpeed value to zero
  analogWrite(carpwm, carZero);
    
  countA = highsPercycle1();                    // Initiallized both of the function located below the void loop
  countB = highsPercycle2();
  delay(systemDelay);

  while (countA >= 1 && countB <= 0) {          // SWITCH 1 IS ON AND SWITCH 2 IS OFF (6-8-22)

    digitalWrite(cardir, HIGH);

    if (serialOn > 0) {                        // This IF statement prints out carSpeed, CountA & CountB values if TRUE

      Serial.print(" carSpeed value is ");
      Serial.print(carSpeed);
      
      Serial.print("  , CountA = ");
      Serial.print(countA);
      //Serial.print("\n");
      Serial.print(" , CountB = ");
      Serial.print(countB); 
      //Serial.print("\n");
      //Serial.print(" , Dir: ");
      //Serial.print(cardir);
      Serial.println();
    }
    if (carSpeed < topSpeed);
    {
      carSpeed++;
      analogWrite(carpwm, carSpeed);

    }

    countA = highsPercycle1();                // checks the highValue, if changed then the while statement will be exited
    countB = highsPercycle2();
    delay(systemDelay); // Moved 4-17-22
    
    if (carSpeed >= topSpeed) {
      carSpeed = topSpeed;
    }

  }
                                                                                           
  carSpeed = carZero;                         // Initially sets the carSpeed Value to zero
  analogWrite(carpwm, carZero);

  countA = highsPercycle1();                  // Runs both function located below the void loop
  countB = highsPercycle2();
  delay(systemDelay);

  while (countB >= 1 && countA <= 0) {        // SWITCH 1 IS OFF AND SWITCH 2 IS ON (6-8-22)
    //digitalRead(PulseInA);
    //digitalRead(PulseInB);

    digitalWrite(cardir, LOW);                // Sets the car direction, LOW

    if (serialOn > 0) {                       // This IF statement prints out carSpeed, CountA & CountB values if TRUE

      Serial.print(" carSpeed value is ");
      Serial.print(carSpeed);
      
      Serial.print("  , CountA = ");
      Serial.print(countA);
      //Serial.print("\n");
      Serial.print(" , CountB = ");
      Serial.print(countB); 
      //Serial.print("\n");
      //Serial.print(" , Dir: ");
      //Serial.print(cardir);
      Serial.println();
    }
    if (carSpeed < topSpeed);
    {
      carSpeed++;
      analogWrite(carpwm, carSpeed);
    }
    
    countA = highsPercycle1();              // Checks the HighValue again, if changed then the while loop is no longer true
    countB = highsPercycle2();

      delay(carAcc); // Moved 4-17-22
    if (carSpeed >= topSpeed) {
        carSpeed = topSpeed;
      }
    }
 

  carSpeed = carZero;                       // initially sets the carSpeed value to zero
  analogWrite(carpwm, carZero);

  countA = highsPercycle1();                // Intializes the functions from the bottom of code
  countB = highsPercycle2();
  //delay(systemDelay);

  while (countA > 0 && countB > 0) {        // BOTH SWITCHES ARE ON
    //digitalRead(PulseInA);
    //digitalRead(PulseInB);
 
    if (serialOn > 0) {                     // This IF statement prints out carSpeed, CountA & CountB values if TRUE

      Serial.print(" carSpeed value is ");
      Serial.print(carSpeed);
      
      Serial.print("  , CountA = ");
      Serial.print(countA);
      //Serial.print("\n");
      Serial.print(" , CountB = ");
      Serial.print(countB); 
      //Serial.print("\n");
      //Serial.print(" , Dir: ");
      //Serial.print(cardir);
      Serial.println();
    }
    if (carSpeed > minSpeed);
    {
      carSpeed--;
      analogWrite(carpwm,carSpeed);
    }
countA = highsPercycle1();                  // Checks the highValue again, if changed then the while loop will be exited
countB = highsPercycle2();

    delay(carAcc);
    if (carSpeed <= minSpeed){
      carSpeed = minSpeed;
  }
 }

 analogWrite(carpwm, carZero);
 analogWrite(cardir, LOW);               // Modified
 delay(longDelay);                       // preventing any action for 2 seconds
}


//__________________________________________________________________________________________
//FUNCTIONA highsPercylce1() & highsPercycle2() are located down here, outside of the void loop. Both function the same

int highsPercycle1()
{
  int highValue = 0;
  int indexHigh = 1;

  while (indexHigh <= pulseNumber) {                // There are 100 highs @ 10KHz, this will check every 2
    if (digitalRead(PulseInA) == HIGH) {
      highValue ++;
      delayMicroseconds(delayUsec);
    }
    indexHigh++;
  }
  return highValue;                      // returns the amount of times signal is high per cylce
}

//__________________________________________________________________________________________
int highsPercycle2()
{
  int highValue = 0;
  int indexHigh = 1;

  while (indexHigh <= pulseNumber) {
    if (digitalRead(PulseInB) == HIGH) {
      highValue ++;
      delayMicroseconds(delayUsec);
    }
    indexHigh++;
  }
  return highValue;
}
