## GO BABY GO PROJECT 

This Project is ongoing with Christian Brothers University. I was started in 2012 by Dr.Galloway. The documentation in this repository is from the **2023 year**. 

Here are some quick links to some documentation in this repo:

- [[Go Baby Go Paper](https://gitlab.com/jbarrie2/hello-world/-/blob/main/.GoBabyGo/GBG/Go%20baby%20go%20paper.docx)]
- [[Go Baby Go Final Presentation Draft](https://gitlab.com/jbarrie2/hello-world/-/blob/main/.GoBabyGo/GBG/Final%20Presentation%20Draft.pptx)] 
- [[Go Baby Go Poster](https://gitlab.com/jbarrie2/hello-world/-/blob/main/.GoBabyGo/GBG/GO%20baby%20go%20poster.pptx)]
- [[Arduino Code](https://gitlab.com/jbarrie2/hello-world/-/blob/main/.GoBabyGo/GBG/03_28_23/03_28_23.ino)]

### Overview and changes

This project had a few software and hardware updates. There were to new methods introduced to controll the Audi car models and thier Pulse Width Modulated Signals. 

### Hardware changes

There are two different circuits implemented:

- [ ] Circuit utilizing a **voltage regultor** to reduce the input voltage to the Arduino (requires software change shown below)
- [ ] Circuit that utilizes an **RC circuit** in conjunction with a voltage divider (this changes a pulse wave into a near DC signal)



### Software Changes required for first voltage regulator implementation:

The code below is for a function used for the arduino to know when the acceleration pedal was pressed. It did this by observing when the signal went to **high** in cycles. 

```C++
//FUNCTIONA highsPercylce1() & highsPercycle2() are located down here, outside of the void loop. Both function the same

int highsPercycle1()
{
  int highValue = 0;
  int indexHigh = 1;

  while (indexHigh <= pulseNumber) {                // There are 100 highs @ 10KHz, this will check every 2
    if (digitalRead(PulseInA) == HIGH) {
      highValue ++;
      delayMicroseconds(delayUsec);
    }
    indexHigh++;
  }
  return highValue;                      // returns the amount of times signal is high per cylce
}
```

